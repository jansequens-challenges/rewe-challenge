# Java/Spring coding challenge

This challenge is about your skills of the language and framework. Also considering knowledge about architecture, approaches, etc.

By the end of your challenge, the following acceptance criteria have to be met:

- This project consists of api endpoints for saving/retrieving a customer
- Login with name/password credentials
- The repository is connected with a h2 in-memory database and operates on a customer table
- The security is based on a name/password jwt, keep the security simple, no further cases should be covered
- Test the necessary code base

## Additional tasks for you
- Cleanup code
- Improve logic
- Finish `TODO`s

Some classes/methods/member variables should not be touched as they are marked with -> `DO NOT TOUCH`

## Git
Please set up your own repository and commit your changes

## Duration
Limit the time you invest in this project at maximum of 60 minutes, no worries if there are any open tasks left. 

Nothing has to be perfect, rather know what you are doing and explain your approach

## Questionnaire
* Think about the project and the architecture, maybe there are some points you would change and adapt.
* Maybe there are also missing parts which come into your mind during the challenge. Just note them down so we can talk about those

