package code.challenge.demo;

import code.challenge.demo.controller.CustomerController;
import code.challenge.demo.models.Customer;
import code.challenge.demo.service.CustomersServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CustomerController.class)
class CustomersControllerTest {

    @MockBean
    private CustomersServiceImpl customerService;

    @Autowired
    MockMvc mockMvc;

    @Test
    void createCustomerthrowsExceptionWithEmptyName() throws Exception {
        Customer testObject = new Customer();
        mockMvc
                .perform(post("/customer").content(new ObjectMapper().writeValueAsString(testObject)))
                .andExpect(status().is4xxClientError());
    }

    @Disabled // TODO enable again
    @Test
    void createIsSuccess() throws Exception {
        Customer testObject = new Customer(1, "name", "email", "password");
        mockMvc
                .perform(post("/customer").content(new ObjectMapper().writeValueAsString(testObject))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
