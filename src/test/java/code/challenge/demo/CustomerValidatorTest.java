package code.challenge.demo;

import code.challenge.demo.models.Customer;
import code.challenge.demo.models.CustomerDTO;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

public class CustomerValidatorTest {


    @Test
    public void testsIncorrectPassord()
    {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<CustomerDTO>> violations = validator
                .validate(new CustomerDTO("name", "email", "toosimple"));
        Assertions.assertThat(violations.size()).isPositive();
    }
}
