package code.challenge.demo;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.security.Key;
import java.util.Date;

@Slf4j
@Service
public class JwtService {

    // DO NOT TOUCH
    private static final long ONE_YEAR_IN_MILISECONDS = 31536000000L;  // TODO too long, not secure
    // DO NOT TOUCH
    private static final String JWT_SECRET = "ThisIsSecretForJWTHS512SignatureAlgorithmThatMUSTHave512bitsKeySize";

    private static final Key SIGNING_KEY = Keys.hmacShaKeyFor(JWT_SECRET.getBytes());

    public String create(String customerId, String name) {
        return Jwts.builder()
                .claim("customerId", customerId)
                .claim("name", name)
                .setExpiration(new Date(System.currentTimeMillis() + ONE_YEAR_IN_MILISECONDS))
                .setIssuedAt(new Date())
                .signWith(SIGNING_KEY)
                .compact();
    }

    public Claims getClaims(String token) {
        try {
            Jws<Claims> claimsJws = getJwtParser().parseClaimsJws(token);
            return claimsJws.getBody();
        } catch (Exception e) {
            log.error("The received token is invalid");
            throw new ResponseStatusException(HttpStatus.FORBIDDEN); // TODO do something more elegant
        }
    }

    public boolean isValid(String token) {
        var claims = getClaims(token).getExpiration().after(new Date());
        return claims;
    }

    protected JwtParser getJwtParser() {
        return Jwts.parserBuilder().setSigningKey(SIGNING_KEY).build();
    }
}
