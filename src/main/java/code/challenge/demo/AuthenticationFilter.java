package code.challenge.demo;

import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import static code.challenge.demo.constants.SecurityConstants.HEADER_NAME;

@Slf4j
public class AuthenticationFilter extends OncePerRequestFilter {

    protected JwtService jwtService;

    public AuthenticationFilter() {
        this.jwtService = new JwtService();
    }

    @Override
    public void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        String token = request.getHeader(HEADER_NAME);

        if (token == null || !jwtService.isValid(token)) {
            chain.doFilter(request, response);
            return;
        }

        Claims user = jwtService.getClaims(token);
        UsernamePasswordAuthenticationToken authentication = null;
        if (user != null) {
            authentication = new UsernamePasswordAuthenticationToken(user, null, new ArrayList<>());
        }

        SecurityContextHolder.getContext().setAuthentication(authentication);

        chain.doFilter(request, response);
    }

    // DO NOT TOUCH
    public void setSecurityContext(String customerId, String name) {
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(customerId, name, Collections.emptyList());
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
    }
}
