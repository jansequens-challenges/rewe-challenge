package code.challenge.demo;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CustomerPasswordValidator implements ConstraintValidator<CustomerPasswordConstraint, String> {

    @Override
    public void initialize(CustomerPasswordConstraint contactNumber) {
    }

    @Override
    public boolean isValid(String password, ConstraintValidatorContext cxt) {
        return password != null && password.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,20}$");
    }
}
