package code.challenge.demo.service;

import code.challenge.demo.models.Customer;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public interface CustomerService {

    CompletableFuture<Customer> getCustomer(String id);

    CompletionStage<Customer> save(Customer customer);

    CompletableFuture<Customer> login(String name, String password);
}
