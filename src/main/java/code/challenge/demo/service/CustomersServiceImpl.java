package code.challenge.demo.service;

import code.challenge.demo.repository.CustomerRepository;
import code.challenge.demo.models.Customer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

@Slf4j
@RequiredArgsConstructor
@Component
public class CustomersServiceImpl implements CustomerService {

    public final CustomerMessageService messageService;
    public final CustomerRepository customerRepository;

    @Override
    public CompletableFuture<Customer> getCustomer(String id) {
        log.info("getCustomer with id + " + id);
        return CompletableFuture.completedFuture(customerRepository.getById(Integer.parseInt(id)));
    }

    @Override
    public CompletionStage<Customer> save(Customer customer) {
        return CompletableFuture.completedStage(customerRepository.save(customer))
                .handleAsync((savedUser, throwable) -> {
                    if (savedUser == null) {
                        log.error(throwable.getMessage());
                    }
                    messageService.sendEmail();
                    log.debug("Saved a new customer {}", customer.getName()); // don't print a password to the log
                    return savedUser;
                });
    }

    // TODO should be rather done via attemptAuthentication() and successfulAuthentication() - extending UsernamePasswordAuthenticationFilter
    // TODO check password is not stored in plain text
    @Override
    public CompletableFuture<Customer> login(String credentials, String password) {
        log.info("Trying to log in '{}'", credentials); // don't print a password to the log
        return CompletableFuture.completedFuture(customerRepository.findByNameAndPassword(credentials, password).orElse(null));
    }
}
