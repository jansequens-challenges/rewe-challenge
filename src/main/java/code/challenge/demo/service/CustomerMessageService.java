package code.challenge.demo.service;

import org.springframework.stereotype.Component;

// DO NOT TOUCH
@Component
public class CustomerMessageService {

    public void sendEmail() {
        // Assume we are sending an email
    }
}
