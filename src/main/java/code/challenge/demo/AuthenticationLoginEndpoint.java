package code.challenge.demo;

import code.challenge.demo.models.Customer;
import code.challenge.demo.service.CustomersServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;

import static code.challenge.demo.constants.SecurityConstants.HEADER_NAME;

@Slf4j
@RestController
@RequiredArgsConstructor
public class AuthenticationLoginEndpoint {

    private final CustomersServiceImpl customerService;
    private final JwtService jwtService;

    // TODO username and password should be sent in body instead of in request params
    @PostMapping(path = "/login")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<String> login(@RequestParam String name, @RequestParam String password) {
        Customer customer;
        try {
            customer = customerService.login(name, password).get();
        } catch (ExecutionException | InterruptedException e) {
            log.warn("Exception during logging in '{}'", name);
            return ResponseEntity.notFound().build(); // TODO do something more elegant
        }

        if (customer == null) {
            log.warn("User {} not found", name);
            return ResponseEntity.notFound().build(); // TODO do something more elegant
        }

        String newToken = jwtService.create(customer.getId().toString(), customer.getName());

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set(HEADER_NAME, newToken);

        return ResponseEntity.ok()
                .headers(responseHeaders)
                .body("Successful login");
    }
}
