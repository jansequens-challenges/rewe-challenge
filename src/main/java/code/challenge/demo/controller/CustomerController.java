package code.challenge.demo.controller;

import code.challenge.demo.CustomerFactory;
import code.challenge.demo.CustomerNameEmptyException;
import code.challenge.demo.models.Customer;
import code.challenge.demo.models.CustomerDTO;
import code.challenge.demo.service.CustomersServiceImpl;
import org.springframework.core.env.MissingRequiredPropertiesException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.NoSuchElementException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

@Controller
@ResponseBody
public class CustomerController {

    protected CustomersServiceImpl customerService;
    private final CustomerFactory customerFactory;

    public CustomerController(CustomersServiceImpl customerService) {
        this.customerService = customerService;
        customerFactory = new CustomerFactory(); // TODO access CustomerFactory statically
    }

    // TODO !!! endpoints are not secured, fix security config

    // TODO throws EntityNotFoundException
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/customers/{customerId}")
    public CompletableFuture<ResponseEntity<CustomerDTO>> getCustomer(@PathVariable String customerId) {
        return customerService.getCustomer(customerId)
                .thenApply(customer -> {
                            if (customer == null) {
                                throw new NoSuchElementException("customer not found");
                            }
                            return customer;
                        })
                .thenCompose(object -> {
                    var dto = customerFactory.createDTOFromCustomer(object);
                    return CompletableFuture.completedFuture(ResponseEntity.ok(dto));
                });
    }

    // TPDP
    @PreAuthorize("isAuthenticated()")
    @PostMapping("/customers")
    @ResponseStatus(HttpStatus.OK)
    public CompletionStage<ResponseEntity<Customer>> save(@RequestBody CustomerDTO newDTO) throws Exception {
        return customerService.save(validateCustomer(newDTO))
                .thenApply(ResponseEntity::ok);
    }

    // TODO should be using Hibernate validator instead
    private Customer validateCustomer(CustomerDTO customer) throws Exception { // TODO don't throw generic Exception
        if (customer.getFullName() == null || customer.getFullName().isEmpty()) {
            throw new CustomerNameEmptyException();
        }
        if (customer.password.isEmpty()) {
            throw new MissingRequiredPropertiesException();
        }
        Customer newCustomer = new Customer(customer.getFullName(), customer.getEmail(), customer.password);
        return newCustomer;
    }
}
