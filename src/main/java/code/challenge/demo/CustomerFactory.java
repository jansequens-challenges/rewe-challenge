package code.challenge.demo;

import code.challenge.demo.models.Customer;
import code.challenge.demo.models.CustomerDTO;

// TODO why is this called a "factory"
public class CustomerFactory {

    public CustomerDTO createDTOFromCustomer(Customer customer) {
        return new CustomerDTO(customer.getName(), customer.getEmail(), customer.getPassword());
    }
}
