package code.challenge.demo;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.LENGTH_REQUIRED, reason = "Name must not be empty")
public class CustomerNameEmptyException extends Exception {
}
