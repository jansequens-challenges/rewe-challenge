package code.challenge.demo.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@AllArgsConstructor
@Entity
@NoArgsConstructor
@Getter
public class Customer {

    @Id
    @GeneratedValue
    private Integer id;

    private String name;

    private String email;

    private String password;

    public Customer(String name, String email, String password) {
        this.name = name;
        this.email = email;
        this.password = password;
    }
}
