package code.challenge.demo.models;

// TODO is the validation working?

import code.challenge.demo.CustomerPasswordConstraint;
import com.sun.istack.NotNull; // TODO don't use this class
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class CustomerDTO {

    @NotNull
    @Getter
    private String fullName;

    @NotNull
    @Getter
    private String email;

    @CustomerPasswordConstraint
    public String password = ""; // TODO use getter, not public
}
